/*
 * Getting_Started_with_AVR_Read_an_IO.c
 *
 * Created: 3/16/2015 2:35:02
 *  Author: Brandon Ruiz Vasquez
 */ 

#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRB |= (1 << DDB5)|(0 << DDB4)|(0 << DDB3)|(0 << DDB2)|(0 << DDB1)|(0 << DDB0);
	//All Inputs Except Pin5 Of PortB
    while(1)
    {
		if(PINB & (1 <<PINB0)) //If PinB0 is Low
		{
			PORTB = (1 << PORTB5);
			//Pin5 On PortB Turns high
		}
		else
		{
			PORTB = (0 << PORTB5);
			//Else The Pin5 Of PortB Will Be Set To Low
		}
		_delay_ms(10);
        //TODO:: Please write your application code 
    }
}